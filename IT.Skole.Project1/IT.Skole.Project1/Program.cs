﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IT.Skole.Project1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Brugeroplysninger";

            Console.WriteLine("Hvad er dit navn?");
            String name = Console.ReadLine();

            Console.Clear();
            Console.WriteLine("Hvad er dit efternavn?");
            String lastName = Console.ReadLine();

            goAge:
            Console.Clear();
            Console.WriteLine("Hvor gamle er du?");
            int age;

            if (!int.TryParse(Console.ReadLine(), out age)) {
                goto goAge;
                return;
            }

            goOld:
            Console.Clear();
            Console.WriteLine("Hvor gamle skal du være før du mener du er gamle?");
            int old;

            if (!int.TryParse(Console.ReadLine(), out old))
            {
                goto goOld;
                return;
            }

            goGender:
            Console.Clear();
            Console.WriteLine("Hvilket Køn er du? (M/D)");
            char gender;

            if (!char.TryParse(Console.ReadLine(), out gender))
            {
                goto goGender;
                return;
            }

            Console.Clear();
            Console.WriteLine("Dit navn er: " + name + " " + lastName);
            Console.WriteLine("Du er: " + age + " gamle.");
            Console.WriteLine("Dit køn: " + gender + " (" + gender.ToString().ToUpper().Replace("M", "Mand").Replace("D", "Dame") + ")");
            if(age >= old ) {
                Console.WriteLine("Du en gamle " + gender.ToString().ToUpper().Replace("M", "mand.").Replace("D", "dame."));
            }
            else
            {
                int result = old - age;
                int year = DateTime.Today.Year + result;
                String getDate = DateTime.Today.Day + "/" + DateTime.Today.Month + "/" + year;
                Console.WriteLine("Du mener man er gamle når man er: " + old + ", og derfor " + result + " år til du bliver gamle. År du bliver så gamle (" + getDate + ")");
            }
            

            Console.ReadKey();
           
        }
    }
}

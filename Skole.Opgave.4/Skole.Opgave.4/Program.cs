﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skole.Opgave._4
{
    class Program
    {
        //Gemmer resultet i her bagefter
        private static String s2;
        //Gemmer det sidste result
        private static int svar;

        static void Main(string[] args)
        {
            Console.Title = "Lommeregner";

        fejl1:
            Console.WriteLine("Skriv dit første tal ind!");
            int t1;
            if (!int.TryParse(Console.ReadLine(), out t1))
            {
                goto fejl1;
                return;
            }

        fejl2:
            Console.Clear();
            Console.WriteLine("Skriv +, -, / eller *");
            String r = Console.ReadLine();

            if (!r.Equals("+") && !r.Equals("-") && !r.Equals("/") && !r.Equals("*"))
            {
                goto fejl2;
                return;
            }

        fejl3:
            Console.Clear();
            Console.WriteLine("Skriv dit andet tal ind!");
            int t2;
            if (!int.TryParse(Console.ReadLine(), out t2))
            {
                goto fejl3;
                return;
            }

            //svaret
            int s;

            Lommeregner(r, t1, t2);

        }

   

    //R er det parementer som står for regneregle (+, -, / og *) osv) 
    //t1 er tal 1 og t2 er tal2
    private static void Lommeregner(String r, int t1, int t2) {

        //svaret
        int s;

        switch (r)
        {
            case "+":
                s = t1 + t2;
                svar = s;
                s2 = t1.ToString() + " + " + t2.ToString() + " = " + s.ToString();
                Console.WriteLine("{0} {1} {2} = {3}", t1, r, t2, s);
                Console.WriteLine("Tryk på enter eller alle andre knapper for at forsæt, eller skriv clear for at starte forfra!");
                Console.ReadKey();
                Forset();
                break;

            case "-":
                s = t1 - t2;
                svar = s;
                s2 = t1.ToString() + " - " + t2.ToString() + " = " + s.ToString();
                Console.WriteLine("{0} {1} {2} = {3}", t1, r, t2, s);
                Console.WriteLine("Tryk på enter eller alle andre knapper for at forsæt, eller skriv clear for at starte forfra!");
                Console.ReadKey();
                Forset();
                break;

            case "/":
                s = t1 / t2;
                svar = s;
                s2 = t1.ToString() + " / " + t2.ToString() + " = " + s.ToString();
                Console.WriteLine("{0} {1} {2} = {3}", t1, r, t2, s);
                Console.WriteLine("Tryk på enter eller alle andre knapper for at forsæt, eller skriv clear for at starte forfra!");
                Console.ReadKey();
                Forset();
                break;

            case "*":
                s = t1 * t2;
                svar = s;
                s2 = t1.ToString() + " * " + t2.ToString() + " = " + s.ToString();
                Console.WriteLine("{0} {1} {2} = {3}", t1, r, t2, s);
                Console.WriteLine("Tryk på enter eller alle andre knapper for at forsæt, eller skriv clear for at starte forfra!");
                Console.ReadKey();
                Forset();
                break;

        }

    }


    private static void Forset() {

    fejl1:
        Console.Clear();
        Console.WriteLine("Skriv +, -, / eller *");
        String r = Console.ReadLine();

        if (!r.Equals("+") && !r.Equals("-") && !r.Equals("/") && !r.Equals("*"))
        {
            goto fejl1;
            return;
        }

    fejl2:
        Console.Clear();
        Console.WriteLine("Skriv det næste tal!");
        int t3;
        if (!int.TryParse(Console.ReadLine(), out t3))
        {
            goto fejl2;
            return;
        }

        //svaret
        int s;

        switch (r)
        {
            case "+":
                s = svar + t3;
                Console.WriteLine("{0} {1} {2} = {3}", s2, r, t3, s);
                Console.ReadKey();
                s2 = s2 + " + " + t3.ToString() + " = " + s.ToString();
                svar = s;
                Forset();
                break;

            case "-":
                s = svar - t3;
                Console.WriteLine("{0} {1} {2} = {3}", s2, r, t3, s);
                Console.ReadKey();
                s2 = s2 + " - " + t3.ToString() + " = " + s.ToString();
                svar = s;
                Forset();
                break;

            case "/":
                s = svar / t3;
                Console.WriteLine("{0} {1} {2} = {3}", s2, r, t3, s);
                Console.ReadKey();
                s2 = s2 + " / " + t3.ToString() + " = " + s.ToString();
                svar = s;
                Forset();
                break;

            case "*":
                s = svar * t3;
                Console.WriteLine("{0} {1} {2} = {3}", s2, r, t3, s);
                Console.ReadKey();
                s2 = s2 + " * " + t3.ToString() + " = " + s.ToString();
                svar = s;
                Forset();
                break;

        }

    }
}

    }


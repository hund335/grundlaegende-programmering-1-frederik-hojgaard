﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skole.Opgave._5
{
    
    class Program
    {
        static void Main(string[] args)
        {
        fejl1:
            Console.Clear();
            Random r = new Random();
            int x = r.Next(1, 1000000001);
            int y = r.Next(1, 1000000001);
            int d = r.Next(1, 1000000001);

            try {
                Console.WriteLine("X: {0} + D: {1} = at skulle gå {3} skridt\n for at komme over Y: {2}", x, d, y, Udregning(x, y, d));
            }catch(Exception e)
            {
                goto fejl1;
                return;
            }
            Console.ReadKey();
        }

        private static int Udregning(int x, int y, int d) {
            int a = 0; //antal/counter

            if (x > y)
               throw new ArgumentException("Fejl X er højre");

            do
            {
                
                a++;
                x += d;

            } while (x < y);
            return a;
        }

       
        private static void Input() {
            Console.Title = "En frø skal kryds en vej";

        fejl1:
            Console.Clear();
            Console.WriteLine("Skriv først X positionen ind (din lokation).");
            int x = 0;

            try
            {
                x = Convert.ToInt32(Console.ReadLine());
            }
            catch (Exception e) {
                Console.WriteLine(e.StackTrace);
                Console.ReadKey();
                goto fejl1;
                return;
            }

        fejl2:
            Console.Clear();
            Console.WriteLine("Skriv først Y positionen ind .");
            int y = 0;
            try
            {
                y = Convert.ToInt32(Console.ReadLine());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                Console.ReadKey();
                goto fejl2;
                return;
            }

        fejl3:
            Console.Clear();
            Console.WriteLine("Skriv først D værdien ind.");
            int d = 0;
            try
            {
                d = Convert.ToInt32(Console.ReadLine());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                Console.ReadKey();
                goto fejl3;
                return;
            }
            Console.Clear();

            Console.WriteLine("X: {0} + D: {1} = at skulle gå {3} skridt for at komme over Y: {2}", x, d, y, Udregning(x, y, d));
            Console.ReadKey();

        }

    }
}

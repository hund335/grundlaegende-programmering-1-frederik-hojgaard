﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skole.Opgave._6
{
    class Program
    {
        static void Main(string[] args)
        {
            // Array[] a = new Array[0];
            Random rnd = new Random();

            int[] a = new int[100];

            for (int i = 0; i < a.Count(); i++)
            {
                int random = rnd.Next(-1000, 1001);
                a[i] = random;
            }
            Routation(a, 2);

        }

        private static void Routation(int[] a, int k) {

            int[] liste = new int[a.Count()];
            int c = 0; //tæller/counter

            do {
                c++;

                for (int i = 0; i < a.Count(); i++) {

                    if (i == 0) {
                        liste[0] = a[a.Count() - 1];

                    } else {
                        liste[i] = a[i - 1];
                    }
                }
                foreach (int i in a) {
                    Console.WriteLine("Old A: " + i);
                }
                Console.WriteLine("");
                liste.CopyTo(a, 0);
         
            } while (c < k);
            Console.ReadKey();



        }

    }
}

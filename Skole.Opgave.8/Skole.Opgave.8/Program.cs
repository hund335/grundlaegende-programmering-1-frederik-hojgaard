﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skole.Opgave._8
{
    class Program
    {
        private static String Username = null;

        static void Main()
        {
            if (Username == null)
            {
                Console.Title = "Regnespil - Login";
                Console.WriteLine("Skriv venligst dit username!");
                String user = Console.ReadLine();
                Username = user;
                Main();
            }

            Console.Clear();
            Console.Title = "Regnespil - Hovedmenu";
            Console.WriteLine("Vælg hvilken regne spil du vil ind i:");
            Console.WriteLine("Skriv 1: for at komme ind i additions spillet!");
            Console.WriteLine("Skriv 2: for at komme ind i subtraktions spillet!");
            Console.WriteLine("Skriv 3: for at komme ind i multiplikations spillet!");
            Console.WriteLine("Skriv 4: for at komme ind i divisons spillet!");
            Console.WriteLine("Skriv 5: for at komme ind i tablel menuen!");
            Console.WriteLine("Skriv 6: for at se highscore listen!");
            Console.WriteLine("(Tryk eller skriv alt andet end det for at at afslutte menuen)");

            String option = Console.ReadLine();

            switch (option)
            {

                case "1":
                    Spil("+");
                    break;
                case "2":
                    Spil("-");
                    break;
                case "3":
                    Spil("*");
                    break;
                case "4":
                    Spil("/");
                    break;
                case "5":
                    Tabel();
                    break;
                case "6":
                    Hightscore();
                    break;
                default:
                    break;
            }


        }

        //Custom List strictur til at Gemme både scores og navn i en field lidt ligesom et HashMap i Java

        public struct Both
        {

            public String getString { get; private set; }
            public int getInt { get; private set; }

            public Both(String name, int score)
            {
                getString = name;
                getInt = score;
            }
        }


        private static void Hightscore()
        {
            Console.Clear();
            Console.Title = "Top 3 - Highscores";
            Console.WriteLine("Top 3 - Highscores\n");
            List<Both> top = new List<Both>();

            using (StreamReader sw = new StreamReader("C:\\Scores.csv"))
            {

                String[] liste = sw.ReadToEnd().Split(';');

                for (int i = 0; i <= liste.Count(); i += 2)
                {
                    if (i + 1 < liste.Count())
                    {

                        top.Add(new Both(liste[i], int.Parse(liste[i + 1])));
                    }
                }

                var topScores = top.OrderByDescending(x => x.getInt).ToList();

                if (top.Count >= 1)
                {

                    string top1 = topScores.Skip(0).First().getString.ToString() + " - " + topScores.Skip(0).First().getInt.ToString();
                    Console.WriteLine("1. " + top1.Replace("\n", "1. ") + "");
                }
                else
                {
                    Console.WriteLine("1. Ingen");
                }

                if (top.Count >= 2)
                {
                    string top2 = topScores.Skip(1).First().getString.ToString() + " - " + topScores.Skip(1).First().getInt.ToString();
                    Console.WriteLine("2. " + top2.Replace("\n", "2. ") + "");
                }
                else
                {
                    Console.WriteLine("2. Ingen");
                }

                if (top.Count >= 3)
                {
                    string top3 = topScores.Skip(2).First().getString.ToString() + " - " + topScores.Skip(2).First().getInt.ToString();
                    Console.WriteLine("3. " + top3.Replace("\n", "3. ") + "");
                }
                else
                {
                    Console.WriteLine("3. Ingen");
                }

                Console.WriteLine("\n(Tryk på en knap for at komme tilbage til hovedmenuen!)");

                String exit = Console.ReadLine();
                Main();
            }
        }


        private static void Tabel()
        {
            Console.Title = "Regnespil - Øvelse af tabeller!";
            int tabel = 0;
            int up = 2;

            error1:
            Console.Clear();
            Console.WriteLine("Skriv hvilken tal tabel du ønsker dig at øve!");
            if (!int.TryParse(Console.ReadLine(), out tabel))
            {
                goto error1;
                return;
            }



            error2:
            Console.Clear();
            int input = 0;
            next:

            Console.WriteLine("Skriv det næste tal i " + tabel + " tabellen!");
            if (!int.TryParse(Console.ReadLine(), out input))
            {
                goto error2;
                return;
            }

            if (input == tabel * up)
            {
                up += 1;

                Console.WriteLine("Ja korrekt, " + input + " var det næste tal i " + tabel + " tabellen!");
                goto next;
            }
            else
            {
                Console.WriteLine("Nej forkert, " + input + " var ikke det næste tal i " + tabel + " tabellen!");
            }

            Console.ReadKey();
        }

        private static void Spil(String regn)
        {
            int questions = 10;
            int score = 0;
            int answer = 0;
            int level = 0;
            char calc;

            error1:
            Console.Title = "Regnespil - Spilmenu";
            Console.Clear();
            Console.WriteLine("Skriv hvilket niveua du vil tage det på: (1, 2, 3 eller 4)");

            if (!int.TryParse(Console.ReadLine(), out level))
            {
                goto error1;
                return;
            }


            error2:
            Console.WriteLine("Skal det være + eller - tal?");
            if (!char.TryParse(Console.ReadLine(), out calc))
            {
                goto error2;
                return;
            }

            next:
            Console.Title = "Regnespil - " + questions.ToString() + " spørgsmål tilbage!";

            if (questions >= 1)
            {
                int tries = 3;

                Random r = new Random();
                int n1 = 0;
                int n2 = 0;

                if (calc.Equals('+'))
                {
                    switch (level)
                    {
                        case 1:
                            n1 = r.Next(1, 10);
                            n2 = r.Next(1, 10);
                            break;
                        case 2:
                            n1 = r.Next(1, 100);
                            n2 = r.Next(1, 100);
                            break;
                        case 3:
                            n1 = r.Next(1, 1000);
                            n2 = r.Next(1, 1000);
                            break;
                        case 4:
                            n1 = r.Next(1, 10000);
                            n2 = r.Next(1, 10000);
                            break;
                        default:
                            goto error1;
                            break;

                    }
                }
                else
                {
                    switch (level)
                    {
                        case 1:
                            n1 = r.Next(-10, -1);
                            n2 = r.Next(-10, -1);
                            break;
                        case 2:
                            n1 = r.Next(-100, -1);
                            n2 = r.Next(-100, -1);
                            break;
                        case 3:
                            n1 = r.Next(-1000, -1);
                            n2 = r.Next(-1000, -1);
                            break;
                        case 4:
                            n1 = r.Next(-10000, -1);
                            n2 = r.Next(-10000, -1);
                            break;
                        default:
                            goto error1;
                            break;
                    }
                }

                switch (regn)
                {
                    case "+":
                        answer = n1 + n2;
                        break;
                    case "-":
                        answer = n1 - n2;
                        break;
                    case "*":
                        answer = n1 * n2;
                        break;
                    case "/":
                        answer = n1 / n2;
                        break;
                }

                Console.WriteLine("Første spørgs mål hvad er: " + n1 + " " + regn + " " + n2);
                Console.WriteLine("Skriv dit svar ind:  (Skriv quit - for at komme tilbage til hoved menuen)");
                error3:
                String input = Console.ReadLine();

                if (input.Equals("quit", StringComparison.InvariantCultureIgnoreCase))
                {
                    Main();
                }

                int ya = 0;

                if (!int.TryParse(input, out ya))
                {
                    goto error3;
                    return;
                }

                if (ya == answer)
                {
                    Console.Clear();
                    questions -= 1;
                    score += 1;
                    goto next;
                }
                else
                {
                    Console.WriteLine("Forkert svar. Du har nu " + tries + " forsøg tilbage, før den går til næste spørgsmål!");
                    if (tries > 0)
                    {
                        tries -= 1;
                        goto error3;
                    }
                    else
                    {
                        Console.Clear();
                        questions -= 1;
                        goto next;
                    }
                }

                Console.ReadKey();
            }
            else
            {
                Console.WriteLine("Du fik en score på " + score.ToString());
                Console.WriteLine("Tryk på enter for at gå til hovedmenuen");

                try
                {
                    FileStream f = new FileStream("C:\\Scores.csv", FileMode.Append);

                    using (StreamWriter w = new StreamWriter(f))
                    {
                        
                        w.WriteLine(Username + ";" + score.ToString() + ";");
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            

                String enter = Console.ReadLine();
                Main();

            }
        }
    }
}

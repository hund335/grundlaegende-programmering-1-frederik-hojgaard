﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skole.Opgave.Ludo
{
    class Dice
    {

        public String roll()
        {
            Random rnd = new Random();
            int get = rnd.Next(1, 7);

            switch (get)
            {
                case 1:
                    return "1";
                case 2:
                    return "2";
                case 3:
                    return "Globus";
                case 4:
                    return "4";
                case 5:
                    return "Stjerne";
                case 6:
                    return "6";
            }
            return "error";
        }

    }
}

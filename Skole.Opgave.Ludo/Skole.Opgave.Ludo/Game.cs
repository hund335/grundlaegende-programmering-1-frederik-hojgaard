﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Skole.Opgave.Ludo
{
    class Game
    {
        private static Team t = new Team();
        private static Dice d = new Dice();
        private static Tokens tk = new Tokens();
        private static Field f = new Field();
        private static int up = 3;


        public void gameRound(int id)
        {
            id = id;
            int number = id + 1;
            String prefix = "Spiller #" + number + "> ";
            Console.Title = "Ludo - Team " + t.getTeam(id) + "`s tur";
            Console.WriteLine("#--------------------------------------------#");
            Console.WriteLine(prefix + "Det er " + t.getTeam(id) + "`s tur.\n");
            Console.WriteLine("\nNummer | Felt | Sted | Team");

            int nr = 1;

            for (int i = 0; i < tk.tokenSize(); i++)
            {

                if (nr > 4)
                {
                    nr = 1;
                    Console.WriteLine("");
                }
                Console.WriteLine("Brik " + nr + ": " + tk.getField(i) + " | " + tk.getType(i) + " - " + tk.getTeam(i));
                nr += 1;
            }

            Console.WriteLine("#--------------------------------------------#");
            Console.WriteLine(prefix + "Først tryk på enter for at at slå med terningen!");
            String enter1 = Console.ReadLine();
            Console.WriteLine(prefix + "Slår med terningen....");
            Thread.Sleep(30);
            String roll = d.roll();
            error1:
            Console.WriteLine(prefix + "Du slog en " + roll + " (Skriv nu ind hvilken prik du ønsker at flyt)");
            int token = 0;
            if (!int.TryParse(Console.ReadLine(), out token)){
                goto error1;
                return;
            }
            
            if(token < 1 || token > 4)
            {
                goto error1;
                return;
            }
            if (id == 0) {
                token = token - 1;
            }
            else
            {
            
                token = token + up;
                up += 4;

            }

            if (tk.getType(token).Equals("Home") && roll.Equals("Globus"))
            {
                if (t.getTeam(id).Equals("Red", StringComparison.InvariantCultureIgnoreCase))
                {
                    Console.WriteLine(prefix + "brikken er nu flyttet ud på field 0 (" + f.getFieldType(0) + ")");
                    tk.moveToken(t.getTeam(id), token, "Normal", 0);
                }
                else if (t.getTeam(id).Equals("Green", StringComparison.InvariantCultureIgnoreCase))
                {
                    Console.WriteLine(prefix + "brikken er nu flyttet ud på field 13 (" + f.getFieldType(13) + ")");
                    tk.moveToken(t.getTeam(id), token, "Normal", 13);
                }
                else if (t.getTeam(id).Equals("Yellow", StringComparison.InvariantCultureIgnoreCase))
                {
                    Console.WriteLine(prefix + "brikken er nu flyttet ud på field 26 (" + f.getFieldType(26) + ")");
                    tk.moveToken(t.getTeam(id), token, "Normal", 26);
                }
                else if (t.getTeam(id).Equals("Blue", StringComparison.InvariantCultureIgnoreCase))
                {
                    Console.WriteLine(prefix + "brikken er nu flyttet ud på field 39 (" + f.getFieldType(39) + ")");
                    tk.moveToken(t.getTeam(id), token, "Normal", 39);
                }
                }
            else if (tk.getType(token).Equals("Normal") && !roll.Equals("Globus") && !roll.Equals("Stjerne"))
            {
                int nloc = tk.getField(token) + Convert.ToInt32(roll);
                Console.WriteLine(prefix + "Prik #" + token + " er gået fra " + tk.getField(token) + " til " + nloc);
                tk.moveToken(t.getTeam(id), token, "Normal", nloc);

            }
            else
            {
                Console.WriteLine(prefix + "Du kan dsv kun rykke en brik ud fra home med en Globus");
            }

            Console.WriteLine("(Tryk på enter for at afslut din tur)");
            String turn = Console.ReadLine();
            if (id < t.teamSize() - 1)
            {
                Console.WriteLine("\n\n");
                gameRound(id + 1);
                token = 0;
            }
            else
            {
                Console.WriteLine("\n\n");
                up = 3;
                gameRound(0);
                token = 0;

            }

        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skole.Opgave.Ludo
{
     class Setup
    {
        private static Team t = new Team();
        private static Tokens tk = new Tokens();
        private static Field f = new Field();
        private static Game g = new Game();
        private static int id = 0;
        private static int ais = 0;
        private int spillere = 0;


        public void Run()
        {
            Console.Title = "Ludo - Setup";
            Console.Clear();
            Console.WriteLine("System > Indtast hvor mange spillere der skal være med (2-4)");

            if (!int.TryParse(Console.ReadLine(), out spillere))
            {
                Run();
                return;
            }
            if (spillere >= 2 && spillere <= 4)
            {
                amountofAIs();
            }
            else
            {
                Console.WriteLine("Fejl > Du kan kun vælge i mellem (2-4)");
                Console.WriteLine("(Tryk på enter for at gå tilbage)");
                String skib = Console.ReadLine();
               Run();
            }
        }

        public void amountofAIs()
        {
            Console.Clear();
            Console.WriteLine("System > Indtast hvor mange AIs der skal være med (0-2)");

            if (!int.TryParse(Console.ReadLine(), out ais))
            {
                amountofAIs();
                return;
            }
            if (ais >= 0 && ais <= 2)
            {
                createPlayers(spillere);
            }
            else
            {
                Console.WriteLine("Fejl > Du kan kun vælge i mellem (0-2)");
                Console.WriteLine("(Tryk på enter for at gå tilbage)");
                String skib = Console.ReadLine();
                amountofAIs();
            }
        }


        public void createPlayers(int amount)
        {
            for (int i = 0; i < amount; i++)
            {
                Error:
                Console.Clear();
                Console.WriteLine("Spiller #" + (i+1) + "> Skriv hvilket team du ønsker at vælge");
                String t1 = Console.ReadLine();
                if (!t.teamisVaild(t1))
                {
                    goto Error;
                }
                else
                {
                    t.addPlayer(t1, id);
                    tk.setTokens(t1, id);
                    id += 1;
                }
            }
            id = 0;
            f.SetupPlayboard();
            g.gameRound(id);

            Console.ReadKey();
        }



    }
}

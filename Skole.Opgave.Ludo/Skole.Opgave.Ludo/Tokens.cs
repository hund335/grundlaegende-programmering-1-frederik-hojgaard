﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skole.Opgave.Ludo
{
    class Tokens
    {
        private static List<Token> tokens = new List<Token>();
        private static List<SingleToken> st = new List<SingleToken>();

        public struct Token {
            public String teams { get; private set; }
            public int amounts { get; private set; }

            public Token(String team, int amount)
            {
                teams = team;
                amounts = amount;
            }
        }

        public struct SingleToken
        {
            public String teams { get; private set; }
            public int ids { get; private set; }
            public String locations { get; private set; }
            public int fields { get; private set; }

            public SingleToken(String team, int id, String location, int field)
            {
                teams = team;
                ids = id;
                locations = location;
                fields = field;
            }
        }

        //Home, Normal and 3rd

        public void setTokens(String team, int playerid)
        {
            if (!Tokens.Equals(team, StringComparer.CurrentCultureIgnoreCase) == true)
            {
                tokens.Add(new Token(team, 4));
                st.Add(new SingleToken(team, 1, "Home", 1));
                st.Add(new SingleToken(team, 2, "Home", 2));
                st.Add(new SingleToken(team, 3, "Home", 3));
                st.Add(new SingleToken(team, 4, "Home", 4));

               // Console.WriteLine("De 4 brikker er blevet sat på home " + "Team #" + team);
            }
        }

        public void moveToken(String team, int id, String type, int loc)
        {
            st[id] = new SingleToken(team, id, type, loc);
        }

        public String getType(int token)
        {
           return st[token].locations;
        }

        public int getField(int token)
        {
            return st[token].fields;
        }

        public String getTeam(int token)
        {
            return st[token].teams;
        }

        public int tokenSize()
        {
            return st.Count;
        }

        public void tokensLeft(String team)
        {

        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Skole.opgave._2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Table";
            for (int i = 1; i <= 10; i += 1)
            {
                Console.Write(i.ToString().PadLeft(4));
            }
            Console.Write("\n");
            for (int i = 2; i <= 20; i += 2)
            {
                Console.Write(i.ToString().PadLeft(4));
            }
            Console.Write("\n");
            for (int i = 3; i <= 30; i += 3)
            {
                Console.Write(i.ToString().PadLeft(4));
            }
            Console.Write("\n");
            for (int i = 4; i <= 40; i += 4)
            {
                Console.Write(i.ToString().PadLeft(4));
            }
            Console.Write("\n");
            for (int i = 5; i <= 50; i += 5)
            {
                Console.Write(i.ToString().PadLeft(4));
            }
            Console.Write("\n");
            for (int i = 6; i <= 60; i += 6)
            {
                Console.Write(i.ToString().PadLeft(4));
            }
            Console.Write("\n");
            for (int i = 7; i <= 70; i += 7)
            {
                Console.Write(i.ToString().PadLeft(4));
            }
            Console.Write("\n");
            for (int i = 8; i <= 80; i += 8)
            {
                Console.Write(i.ToString().PadLeft(4));
            }
            Console.Write("\n");
            for (int i = 9; i <= 90; i += 9)
            {
                Console.Write(i.ToString().PadLeft(4));
            }
            Console.Write("\n");
            for (int i = 10; i <= 100; i += 10)
            {
                Console.Write(i.ToString().PadLeft(4));
            }
            Console.ReadKey();

        }
    }
}
